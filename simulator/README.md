# BTC REST API simulator

Simulates REST API for testing purposes.

After starting `BtcWsSimulatorApplication` this application will listen to post requests on port 8088 (configurable).

## MeterValuesService

The BTC MeterValuesService endpoint is reachable via `http://localhost:9090/MeterValuesService.svc/v2/{tenantName}`

`tenantName` is currently ignored an can be any string value.
