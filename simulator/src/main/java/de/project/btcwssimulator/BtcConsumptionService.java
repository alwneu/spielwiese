package de.project.btcwssimulator;

import static java.util.stream.Collectors.toList;
import static org.slf4j.LoggerFactory.getLogger;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.ws.rs.core.Response;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import de.project.btcwssimulator.request.ConsumptionRequest;
import de.project.btcwssimulator.request.MeterValueOrder;
import de.project.btcwssimulator.response.ConsumptionResponse;
import de.project.btcwssimulator.response.MeasuredValue;
import de.project.btcwssimulator.response.MeterValue;


@RestController
@RequestMapping("/MeterValuesService.svc/v2")
public class BtcConsumptionService {

    private static final Logger LOGGER = getLogger(BtcConsumptionService.class);

    private static final int MAX_KWH_PER_QHOUR = 10;

    @RequestMapping(value = "/{tenantName}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ConsumptionResponse getConsumptions(@PathVariable(value = "tenantName") final String tenantName,
                                               @AuthenticationPrincipal final UserDetails user,
                                               @RequestBody final ConsumptionRequest request) {

        OrderHeader orderHeader = request.getOrderHeader();

        LOGGER.debug("Received order with id [{}].", orderHeader.getOrderId());

        ConsumptionResponse response = new ConsumptionResponse();
        response.setOrderHeader(orderHeader);

        response.setMeterValues(
                request.getMeterValueOrders().stream().map(this::processOrder).collect(Collectors.toList()));

        return response;
    }
    
    @RequestMapping(value = "/{tenantName}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Response getHelloWorld(@PathVariable(value = "tenantName") final String tenantName,
                                               @AuthenticationPrincipal final UserDetails user) {

        LOGGER.debug("Received tenantName [{}].", tenantName);

        return Response.ok(tenantName).build();
    }


    private MeterValue processOrder(final MeterValueOrder order) {
        LOGGER.debug("Generating dummy consumptions for Meter [{} / {}] from [{}] to [{}]...", order.getMeterSerialNo(),
                order.getObisCode(), order.getStartTime(), order.getEndTime());

        MeterValue meterValue = new MeterValue();
        meterValue.setSerialNo(order.getMeterSerialNo());
        meterValue.setUnit("kWh");
        meterValue.setResultInformation("OK");
        meterValue.setObisCode(order.getObisCode());

        meterValue.setMeasuredValues(values(order.getStartTime(), order.getEndTime()));

        return meterValue;
    }

    private List<MeasuredValue> values(final DateTime startTime, final DateTime endTime) {
        long valueCount = new org.joda.time.Duration(startTime, endTime).getStandardMinutes() / 15;
        return Stream.iterate(startTime.plusMinutes(15), d -> d.plusMinutes(15)).limit(valueCount).map(this::dummyValue)
                .collect(toList());
    }

    private MeasuredValue dummyValue(final DateTime date) {
        MeasuredValue measuredValue = new MeasuredValue();
        measuredValue.setMeasurementTime(date);
        measuredValue.setValueStatus(MeasuredValue.Status.M);
        measuredValue.setMeasuredValue(BigDecimal.valueOf(Math.random() * MAX_KWH_PER_QHOUR));
        return measuredValue;
    }

}
