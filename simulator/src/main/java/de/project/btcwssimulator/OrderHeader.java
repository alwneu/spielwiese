package de.project.btcwssimulator;

import com.fasterxml.jackson.annotation.JsonProperty;


public class OrderHeader {

    @JsonProperty("Sender")
    private String sender;

    @JsonProperty("SenderTenant")
    private String senderTenant = "";

    @JsonProperty("OrderId")
    private String orderId;

    @JsonProperty("Receiver")
    private String receiver = "";

    @JsonProperty("Version")
    private String version = "2";

    public OrderHeader() {
    }

    public OrderHeader(final String sender, final String orderId) {
        this.sender = sender;
        this.orderId = orderId;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(final String sender) {
        this.sender = sender;
    }

    public String getSenderTenant() {
        return senderTenant;
    }

    public void setSenderTenant(final String senderTenant) {
        this.senderTenant = senderTenant;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(final String orderId) {
        this.orderId = orderId;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(final String receiver) {
        this.receiver = receiver;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(final String version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "OrderHeader [sender=" + sender + ", senderTenant=" + senderTenant + ", orderId=" + orderId
                + ", receiver=" + receiver + ", version=" + version + "]";
    }

}
