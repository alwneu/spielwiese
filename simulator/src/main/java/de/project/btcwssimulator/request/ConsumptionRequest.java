package de.project.btcwssimulator.request;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import de.project.btcwssimulator.OrderHeader;


public class ConsumptionRequest {

    @JsonProperty("OrderHeader")
    private OrderHeader orderHeader;

    @JsonProperty("MeterValueOrders")
    private List<MeterValueOrder> meterValueOrders;

    public OrderHeader getOrderHeader() {
        return orderHeader;
    }

    public void setOrderHeader(final OrderHeader orderHeader) {
        this.orderHeader = orderHeader;
    }

    public List<MeterValueOrder> getMeterValueOrders() {
        return meterValueOrders;
    }

    public void setMeterValueOrders(final List<MeterValueOrder> meterValueOrders) {
        this.meterValueOrders = meterValueOrders;
    }

    @Override
    public String toString() {
        return "ConsumptionRequest [orderHeader=" + orderHeader + ", meterValueOrders=" + meterValueOrders + "]";
    }

}
