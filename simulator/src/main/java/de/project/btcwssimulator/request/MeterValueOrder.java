package de.project.btcwssimulator.request;

import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonProperty;


public class MeterValueOrder {

    @JsonProperty("MeterSerialNo")
    private String meterSerialNo;

    @JsonProperty("PointOfDelivery")
    private String pointOfDelivery = null;

    @JsonProperty("ObisCode")
    private String obisCode;

    @JsonProperty("PeriodDuration")
    private String periodDuration = "QuarterHour";

    @JsonProperty("ValueKind")
    private String valueKind = "Calibrated";

    @JsonProperty("StartTime")
    private DateTime startTime;

    @JsonProperty("EndTime")
    private DateTime endTime;

    public MeterValueOrder(final String meterSerialNo, final String obisCode, final DateTime startTime,
            final DateTime endTime) {
        this.meterSerialNo = meterSerialNo;
        this.obisCode = obisCode;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public MeterValueOrder() {
    }

    public String getMeterSerialNo() {
        return meterSerialNo;
    }

    public void setMeterSerialNo(final String meterSerialNo) {
        this.meterSerialNo = meterSerialNo;
    }

    public String getPointOfDelivery() {
        return pointOfDelivery;
    }

    public void setPointOfDelivery(final String pointOfDelivery) {
        this.pointOfDelivery = pointOfDelivery;
    }

    public String getObisCode() {
        return obisCode;
    }

    public void setObisCode(final String obisCode) {
        this.obisCode = obisCode;
    }

    public String getPeriodDuration() {
        return periodDuration;
    }

    public void setPeriodDuration(final String periodDuration) {
        this.periodDuration = periodDuration;
    }

    public String getValueKind() {
        return valueKind;
    }

    public void setValueKind(final String valueKind) {
        this.valueKind = valueKind;
    }

    public DateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(final DateTime startTime) {
        this.startTime = startTime;
    }

    public DateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(final DateTime endTime) {
        this.endTime = endTime;
    }

    @Override
    public String toString() {
        return "MeterValueOrder [meterSerialNo=" + meterSerialNo + ", pointOfDelivery=" + pointOfDelivery
                + ", obisCode=" + obisCode + ", periodDuration=" + periodDuration + ", valueKind=" + valueKind
                + ", startTime=" + startTime + ", endTime=" + endTime + "]";
    }

}
