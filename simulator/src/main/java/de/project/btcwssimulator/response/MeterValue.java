package de.project.btcwssimulator.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;


public class MeterValue {

    @JsonProperty("SerialNo")
    private String serialNo;

    @JsonProperty("PointOfDelivery")
    private final String pointOfDelivery = null;

    @JsonProperty("ObisCode")
    private String obisCode;

    @JsonProperty("Unit")
    private String unit;

    @JsonProperty("TariffInformation")
    private final String tariffInformation = null;

    @JsonProperty("ResultInformation")
    private String resultInformation;

    @JsonProperty("MeasuredValues")
    private List<MeasuredValue> measuredValues;

    public MeterValue() {
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(final String serialNo) {
        this.serialNo = serialNo;
    }

    public String getObisCode() {
        return obisCode;
    }

    public void setObisCode(final String obisCode) {
        this.obisCode = obisCode;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(final String unit) {
        this.unit = unit;
    }

    public String getResultInformation() {
        return resultInformation;
    }

    public void setResultInformation(final String resultInformation) {
        this.resultInformation = resultInformation;
    }

    public List<MeasuredValue> getMeasuredValues() {
        return measuredValues;
    }

    public void setMeasuredValues(final List<MeasuredValue> measuredValues) {
        this.measuredValues = measuredValues;
    }

    public String getPointOfDelivery() {
        return pointOfDelivery;
    }

    public String getTariffInformation() {
        return tariffInformation;
    }

    @Override
    public String toString() {
        return "MeterValue [serialNo=" + serialNo + ", pointOfDelivery=" + pointOfDelivery + ", obisCode=" + obisCode
                + ", unit=" + unit + ", tariffInformation=" + tariffInformation + ", resultInformation="
                + resultInformation + ", measuredValues=" + measuredValues + "]";
    }

}
