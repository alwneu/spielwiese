package de.project.btcwssimulator.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import de.project.btcwssimulator.OrderHeader;


public class ConsumptionResponse {

    @JsonProperty("OrderHeader")
    private OrderHeader orderHeader;

    @JsonProperty("MeterValues")
    private List<MeterValue> meterValues;

    public ConsumptionResponse() {
    }

    public OrderHeader getOrderHeader() {
        return orderHeader;
    }

    public void setOrderHeader(final OrderHeader orderHeader) {
        this.orderHeader = orderHeader;
    }

    public List<MeterValue> getMeterValues() {
        return meterValues;
    }

    public void setMeterValues(final List<MeterValue> meterValues) {
        this.meterValues = meterValues;
    }

    @Override
    public String toString() {
        return "ConsumptionResponse [orderHeader=" + orderHeader + ", meterValues=" + meterValues + "]";
    }

}
