package de.project.btcwssimulator.response;

import java.math.BigDecimal;

import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonProperty;


public class MeasuredValue {

    public enum Status {
        M, U, C, S, P, T, I
    }

    @JsonProperty("MeasurementTime")
    private DateTime measurementTime;

    @JsonProperty("MeasuredValue")
    private BigDecimal measuredValue;

    @JsonProperty("ValueStatus")
    private Status valueStatus;

    public MeasuredValue() {
    }

    public DateTime getMeasurementTime() {
        return measurementTime;
    }

    public void setMeasurementTime(final DateTime measurementTime) {
        this.measurementTime = measurementTime;
    }

    public BigDecimal getMeasuredValue() {
        return measuredValue;
    }

    public void setMeasuredValue(final BigDecimal measuredValue) {
        this.measuredValue = measuredValue;
    }

    public Status getValueStatus() {
        return valueStatus;
    }

    public void setValueStatus(final Status valueStatus) {
        this.valueStatus = valueStatus;
    }

    @Override
    public String toString() {
        return "MeasuredValue [measurementTime=" + measurementTime + ", measuredValue=" + measuredValue
                + ", valueStatus=" + valueStatus + "]";
    }

}
