package de.project.btcwssimulator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BtcWsSimulatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(BtcWsSimulatorApplication.class, args);
	}
}
